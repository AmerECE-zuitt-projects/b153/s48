import { Card, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";

const SpecificCourse = () => {
  // e.g
  // courseId: 15651ds454
  const { courseId } = useParams();
  // console.log(courseId);

  const [courseData, setCourseData] = useState([]);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
      .then((res) => res.json())
      .then((data) => {
        setCourseData(data);
        // console.log(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Card className="mt-3">
      <Card.Header className="bg-dark text-white text-center pb-0">
        <h4>{courseData.name}</h4>
      </Card.Header>
      <Card.Body>
        <Card.Text>{courseData.description}</Card.Text>
        <h6>Price: {courseData.price}</h6>
      </Card.Body>
      <Card.Footer className="d-grid gap-2">
        <Button variant="primary" block>
          Enroll
        </Button>
      </Card.Footer>
    </Card>
  );
};

export default SpecificCourse;
